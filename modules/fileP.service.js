const fspromise = require('fs/promises')
const path = require('path')

const filePService = {
    read : async (filename) => {
        const filePath = path.resolve(process.cwd(), 'files', filename )
        console.log("--> FSPROMISE - READ - START");
        try {
            const text = await fspromise.readFile(filePath, { encoding : 'utf-8'})
            console.log("--> FSPROMISE - READ - TXT : ", text);
        } catch(err) {
            console.log(err);
        }
        console.log("--> FSPROMISE - READ - END");

    }, 

    write : async (filename) => {
        const filePath = path.resolve(process.cwd(), 'files', filename )
        const textToAdd = `Kikoo`
        console.log("--> FSPROMISE - WRITE - START");
        try {
            await fspromise.writeFile(filePath, textToAdd, { flag : 'a'})
        }
        catch(err) {
            console.log(err);
        }
        console.log("--> FSPROMISE - WRITE - END");

    },

    open : async (filename) => {
        const filePath = path.resolve(process.cwd(), 'files', filename )

        const buffer = Buffer.alloc(100)
        console.log("--> FSPROMISE - OPEN - START");
        try {
            //Essayer d'ouvrir
            const fd = await fspromise.open(filePath, 'a+')
            console.log("--> FSPROMISE - OPEN - FILE OPENED");
            await fd.read(buffer, 0, buffer.length, 0)
            console.log("--> FSPROMISE - OPEN - FILE CONTENT ", buffer.toString());
            await fd.write('Toujours lurdi')
            await fd.close()
            
        }
        catch(err) {
            console.log(err);
        }

    },

    delete : async (filename) => {
        const filePath = path.resolve(process.cwd(), 'files', filename )
        console.log("--> FSPROMISE - DELETE - START");
        try {
            await fspromise.unlink(filePath)
            console.log("--> FSPROMISE - DELETE - OK");

        }
        catch(err) {
            console.log(err);
            console.log("--> FSPROMISE - DELETE - FAILED");

        }
        console.log("--> FSPROMISE - DELETE - END");

    }
}

module.exports = filePService