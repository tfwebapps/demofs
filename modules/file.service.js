const fs = require("fs")
const path = require("path")

const fileService = {
    read : (filename) => {
        //console.log(__dirname)
        //console.log(process.cwd());
        //Construction du chemin vers le fichire reçu en param
        const filePath = path.resolve(process.cwd(), 'files', filename )
        console.log(filePath)

        //Utilisation de la méthode read de fs
        fs.readFile(filePath, (error, buffer) => {
            console.log("--> FS - READ - START");
            //Si erreur lors de la lecture
            if(error) {
                console.log(error);
                return
            }
            //Si pas d'erreur, on peut lire ce qu'il y dedans
            console.log("----> FS - READ - BUFFER : ", buffer)
            const bufferToTxt = buffer.toString() //buffer.toString('utf-8') <- valeur par défaut
            console.log(bufferToTxt)
        })
    },
    
    write : (filename) => {
        const filePath = path.resolve(process.cwd(), 'files', filename )

        const textToWrite = ` Aujourd'hui nous sommes le ${new Date().toLocaleDateString()}`

        fs.writeFile(filePath, textToWrite, /*{ flag : 'a' } ,*/ (error) => {
            console.log("--> FS - WRITE - START");
            if(error) {
                console.log(error);
                console.log("Arrêt écriture");
                return
            }
            console.log("--> FS - WRITE - WRITE OK")
        })

    },

    open : (filename) => {
        const filePath = path.resolve(process.cwd(), 'files', filename )

        const buffer = Buffer.alloc(100)

        fs.open(filePath, 'a+', (error, fd) => {
            console.log("--> FS - OPEN - START")
            if(error) {
                console.log(error)
                return
            }

            //Si pas d'erreur, on fait toutes les opérations qu'on veut
            //Par ex : lire
            //fd : identifiant du fichier
            //buffer : objet buffer prédéfini, il va aller se remplir pendant l'opération
            //offset : Position de démarrage dans le buffer
            //length : Nb lettres (bytes) encodés
            //position : Position de démarrage dans le fichier
            fs.read(fd, buffer, 0, buffer.length, 0, () => {
                console.log("--> FS - OPEN - START READING")
                console.log("--> FS - OPEN - READING BUFFER : ", buffer)
                const bufferToTxt = buffer.toString()
                console.log("--> FS - OPEN - READING TXT : ", bufferToTxt)              
            })

            fs.write(fd, "C'est lurdi", (error) => {
                if(error) {
                    console.log(error);
                    return
                }
            })

            // fs.close(fd, (error) => {
            //     if(error) {
            //         console.log(error);
            //     }
            //     console.log("--> FS - OPEN - CLOSE FILE")

            // })
        })
    },


    delete : (filename) => {
        const filePath = path.resolve(process.cwd(), 'files', filename )
        fs.unlink(filePath, (error) => {
            if(error) {
                console.log(error);
                return
            }
            console.log("--> DELETE FILE OK");
        })
    }
}

module.exports = fileService