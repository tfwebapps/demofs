
console.log("---- DEMO FILE SYSTEM ----");

const fileService = require("./modules/file.service")
const filePService = require("./modules/fileP.service")
// fileService.read("hello.txt")
// // fileService.write("bonjour.txt")
// fileService.open("bonjour.txt")

// fileService.delete("just4delete.txt")

filePService.read("hello.txt")
// filePService.write("hello.txt")
filePService.open("bonjour.txt")
filePService.delete("bjour.txt")

//#region Apparté sur les promesses
// const division = (nb1 , nb2) => {
//     return new Promise((resolve, reject) => {
//         if(nb2 !== 0) {
//             resolve(nb1/nb2)
//         }
//         else {
//             reject("Division par 0 impossible")
//         }
//     })
// }

// division(12, 0)
//     .then((resultat) => { console.log("Le résultat de la division est : ", resultat);})
//     .catch((error) => {console.log(error)})
//     .finally(() => { console.log("Division terminée, qu'elle ai réussie ou échouée") })

// const operation = async() => {
//     try {
//         const resultat = await division(12, 0)
//         console.log("Le résultat de la division est : ", resultat);

//     }
//     catch(err) {
//         console.log(err)
//     }
//     console.log("Division terminée");
// }
// operation()
// Liens vers demo de l'omelette : https://gist.github.com/AudeBstorm/03e2d0989ac8633b20b8fb46660da6bf
//#endregion
